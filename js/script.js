//setTimeout() - задает время задержки перед выполнение переданной функции один раз, а setInterval() - позволяет циклично выполнять функцию,
//с указанной задержкой времени.
//Нулевая задержка или же планирование позволяет запустить функцию в setTimeout() сразу после выполнения кода после него.
//Необходимо использовать clearTimeout() для остановки запущенного цикла иначе он будет замкнут.
const imgList = document.querySelectorAll('.image-to-show')
const stopBtn = document.querySelector('.stop-btn')
const restartBtn = document.querySelector('.restart-btn')

document.addEventListener('DOMContentLoaded', function () {
    let i = 0
    let timerId = setInterval(function () {
        imgList[i].style.display = 'none'
        i++
        if (i === imgList.length) {
            i = 0
        }
        imgList[i].style.display = 'block'
    }, 3000)
    stopBtn.addEventListener('click',() => {
        clearInterval(timerId)
        stopBtn.disabled = true;
        restartBtn.disabled = false;
    })
    restartBtn.addEventListener('click',() => {
        restartBtn.disabled = true;
        stopBtn.disabled = false;
        timerId = setInterval(() => {
            imgList[i].style.display = 'none'
            i++
            if (i === imgList.length) {
                i = 0
            }
            imgList[i].style.display = 'block'
        }, 3000)
    })
})


